import Vue from 'vue'
import Router from 'vue-router'
import Resource from 'vue-resource'
import ListIssue from '@/components/ListIssue'
import DetailIssue from '@/components/DetailIssue'

Vue.use(Router)
Vue.use(Resource)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ListIssue',
      component: ListIssue
    },
    {
      path: '/list_issue_withdetail/:number',
      name: 'DetailIssue',
      component: DetailIssue
    }
  ]
})
