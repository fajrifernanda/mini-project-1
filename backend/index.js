var express = require('express');

const app = express();

const axios = require('axios');

var cors = require('cors')

app.use(cors())


app.get('/list_issue', (req,res)=>{
    axios.post('https://api.github.com/graphql', {
        query: `query{
            repository(owner:"facebook", name:"react"){
              issues(states:OPEN, first:10, orderBy:{field:CREATED_AT, direction:DESC}){
                nodes{
                  number
                  title
                }
                
              }
            }
          }
          `
      }, {
          headers: {
            'Authorization': 'bearer 9d0035082dce9918c431ec3836d63b7696aec942 '
          }
        }).then((response)=>{
            res.send(response.data)
        });

   
})

app.get('/list_issue_withdetail/:issueNumber', (req,res)=>{
    var issueNum = req.params.issueNumber
    axios.post('https://api.github.com/graphql',{
        query:`
        query{
            repository(owner:"facebook", name:"react"){
              issue(number:${issueNum}){
                author{
                  login
                }
                number
                title
                comments(first:10){
                  nodes{
                    author{
                      login
                    }
                    body
                  }
                }  
              }
            }
          }         
          `
        }, {
            headers: {
              'Authorization': 'bearer 9d0035082dce9918c431ec3836d63b7696aec942 '
            }
          }).then((response)=>{
              res.send(response.data)
          });
        });


app.listen("4000",()=>{
    console.log("backend run on port 4000")
})



